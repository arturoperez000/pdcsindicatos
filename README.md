
requirements:
-nodejs
-angular 7
-mongodb

--------------
### START APPLICATION
--------------

To start the application open a terminal in this folder and type:

``` bash
$ npm install
$ npm start
```

Go to http://localhost:8000

login name: admin
login pass: pass

--------------
### CONFIGURE
--------------

Set bdd properties in:
* `server/properties.js`

