
import Properties from "../../../properties";
import SindicatosModel from "../../../models/Newtest_db/SindicatosModel";
import ErrorManager from "../../../classes/ErrorManager";
import { authorize } from "../../../security/SecurityManager";
import SindicatosController from "../SindicatosController";

const generatedControllers = {
  /**
   * Init routes
   */
  init: router => {
    const baseUrl = `${Properties.api}/Sindicatos`;
    router.post(baseUrl + "", authorize([]), SindicatosController.create);
    router.delete(baseUrl + "/:id", authorize([]), SindicatosController.delete);
    router.get(baseUrl + "", authorize([]), SindicatosController.list);
    router.post(baseUrl + "/:id", authorize([]), SindicatosController.update);
  },


  // CRUD METHODS


  /**
  * SindicatosModel.create
  *   @description CRUD ACTION create
  *   @param Sindicatos obj Object to insert
  *
  */
  create: async (req, res) => {
    try {
      const result = await SindicatosModel.create(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * SindicatosModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id Sindicatos
  *
  */
  delete: async (req, res) => {
    try {
      const result = await SindicatosModel.delete(req.params.id);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * SindicatosModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF Sindicatos
  *
  */
  list: async (req, res) => {
    try {
      const result = await SindicatosModel.list();
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  /**
  * SindicatosModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id Sindicatos
  *   @returns Sindicatos
  *
  */
  update: async (req, res) => {
    try {
      const result = await SindicatosModel.update(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  // Custom APIs

};

export default generatedControllers;
