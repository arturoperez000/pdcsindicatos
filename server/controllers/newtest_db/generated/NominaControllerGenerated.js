
import Properties from "../../../properties";
import NominaModel from "../../../models/Newtest_db/NominaModel";
import ErrorManager from "../../../classes/ErrorManager";
import { authorize } from "../../../security/SecurityManager";
import NominaController from "../NominaController";

const generatedControllers = {
  /**
   * Init routes
   */
  init: router => {
    const baseUrl = `${Properties.api}/nominas`;
    router.post(baseUrl + "", authorize([]), NominaController.create);
    router.delete(baseUrl + "/:id", authorize([]), NominaController.delete);
    router.get(baseUrl + "", authorize([]), NominaController.list);
    router.get(baseUrl + "/:id", authorize([]), NominaController.get);
    router.post(baseUrl + "/export", authorize([]), NominaController.downloadData);
    router.post(baseUrl + "/:id", authorize([]), NominaController.update);
  },


  // CRUD METHODS


  /**
  * nominaModel.create
  *   @description CRUD ACTION create
  *   @param nomina obj Object to insert
  *
  */
  create: async (req, res) => {
    try {
      const result = await NominaModel.create(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * nominaModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id nomina
  *
  */
  delete: async (req, res) => {
    try {
      const result = await NominaModel.delete(req.params.id);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * NominasModel.get
  *   @description CRUD ACTION get
  *   @param ObjectId id Id 
  *
  */
  get: async (req, res) => {
    try {
      const result = await NominaModel.get(req.params.id);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
 
  /**
  * nominaModel.downloadData
  *   @description CRUD ACTION download
  *   @returns String
  *
  */
  downloadData: async (req, res) => {
    try {
      const result = await NominaModel.downloadData(req.body);
      
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * nominaModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF nomina
  *
  */
  list: async (req, res) => {
    try {
      const result = await NominaModel.list();
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  /**
  * nominaModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id nomina
  *   @returns nomina
  *
  */
  update: async (req, res) => {
    try {
      const result = await NominaModel.update(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  // Custom APIs

};

export default generatedControllers;
