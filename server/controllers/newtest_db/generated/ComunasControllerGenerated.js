
import Properties from "../../../properties";
import ComunasModel from "../../../models/Newtest_db/ComunasModel";
import ErrorManager from "../../../classes/ErrorManager";
import { authorize } from "../../../security/SecurityManager";
import ComunasController from "../ComunasController";

const generatedControllers = {
  /**
   * Init routes
   */
  init: router => {
    const baseUrl = `${Properties.api}/comunas`;
    router.post(baseUrl + "", authorize([]), ComunasController.create);
    router.delete(baseUrl + "/:id", authorize([]), ComunasController.delete);
    router.get(baseUrl + "", authorize([]), ComunasController.list);
    router.get(baseUrl + "/:id", authorize([]), ComunasController.get);
    router.post(baseUrl + "/:id", authorize([]), ComunasController.update);
  },


  // CRUD METHODS


  /**
  * comunasModel.create
  *   @description CRUD ACTION create
  *   @param comunas obj Object to insert
  *
  */
  create: async (req, res) => {
    try {
      const result = await ComunasModel.create(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * ComunasModel.get
  *   @description CRUD ACTION get
  *   @param ObjectId id Id 
  *
  */
  get: async (req, res) => {
    try {
      const result = await ComunasModel.get(req.params.id);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * comunasModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id comunas
  *
  */
  delete: async (req, res) => {
    try {
      const result = await ComunasModel.delete(req.params.id);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  /**
  * comunasModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF comunas
  *
  */
  list: async (req, res) => {
    try {
      const result = await ComunasModel.list();
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  /**
  * comunasModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id comunas
  *   @returns comunas
  *
  */
  update: async (req, res) => {
    try {
      const result = await ComunasModel.update(req.body);
      res.json(result);
    } catch (err) {
      const safeErr = ErrorManager.getSafeError(err);
      res.status(safeErr.status).json(safeErr);
    }
  },
  
  
  // Custom APIs

};

export default generatedControllers;
