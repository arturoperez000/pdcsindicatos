// Database
import Database from "../../../classes/Database_Newtest_db";
import mongoose, { Schema } from "mongoose";

// Logger
import Logger from "../../../classes/Logger";

const generatedModel = {
  /**
   * Init  schema
   */
  init() {
    const db = Database.getConnection();

    /**
      * comunas
      */
    const comunasSchema = new mongoose.Schema({
      name: {
        type: "String"
      },
      //RELATIONS
      
      
      //EXTERNAL RELATIONS
      /*
      */
    });

    generatedModel.setModel(db.connection.model("Comunas", comunasSchema));

    return comunasSchema;
  },

  /**
   * Set Model
   */
  setModel: model => {
    generatedModel.model = model;
  },

  /**
   * Get model
   */
  getModel: () => {
    return generatedModel.model;
  },

  // Start queries
    

  // CRUD METHODS


  /**
  * comunasModel.create
  *   @description CRUD ACTION create
  *   @param comunas obj Object to insert
  *
  */
  async create(item) {
    const obj = new generatedModel.model(item);
    return await obj.save();
  },
  
  /**
  * comunasModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id comunas
  *
  */
  async delete(id) {
    return await generatedModel.model.findByIdAndRemove(id);
  },
  
  /**
  * comunasModel.get
  *   @description CRUD ACTION get
  *   @param ObjectId id Id 
  *
  */
  async get(id) {
    return await generatedModel.model.findOne({_id: id});
  },
  
  /**
  * comunasModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF comunas
  *
  */
  async list() {
    return await generatedModel.model.find();
  },
  
  /**
  * comunasModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id comunas
  *   @returns comunas
  *
  */
  async update(item) { 
    return await generatedModel.model.findOneAndUpdate({ _id: item._id }, item, {'new': true});
  },
  
};

export default generatedModel;
