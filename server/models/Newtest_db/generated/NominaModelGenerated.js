// Database
import Database from "../../../classes/Database_Newtest_db";
import mongoose, { Schema } from "mongoose";

// Logger
import Logger from "../../../classes/Logger";

const generatedModel = {
  /**
   * Init  schema
   */
  init() {
    const db = Database.getConnection();

    /**
      * nomina
      */
    const nominaSchema = new mongoose.Schema({
      name: {
        type: "String"
      },
      last: {
        type: "String"
      },
      rut: {
        type: "String"
      },
      dig: {
        type: "String"
      },
      datenac: {
        type: "Date"
      },
      street_num: {
        type: "String"
      },
      comuna: {
        type: "String"
      },
      servicio: {
        type: "String"
      },
      movil: {
        type: "String"
      },
      email: {
        type: "String"
      },
      dateinemp: {
        type: "Date"
      },
      sindicato: {
        type: "String"
      },
      datereg: {
        type: "Date"
      },
      monto: {
        type: "String"
      },

      //RELATIONS
      
      
      //EXTERNAL RELATIONS
      /*
      */
    });

    generatedModel.setModel(db.connection.model("Nomina", nominaSchema));

    return nominaSchema;
  },

  /**
   * Set Model
   */
  setModel: model => {
    generatedModel.model = model;
  },

  /**
   * Get model
   */
  getModel: () => {
    return generatedModel.model;
  },

  // Start queries
    

  // CRUD METHODS


  /**
  * nominaModel.create
  *   @description CRUD ACTION create
  *   @param nomina obj Object to insert
  *
  */
  async create(item) {
    item.datereg = Date.now();
    const obj = new generatedModel.model(item);
    return await obj.save();
  },
  
  /**
  * nominaModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id nomina
  *
  */
  async delete(id) {
    return await generatedModel.model.findByIdAndRemove(id);
  },
  
  /**
  * nominaModel.get
  *   @description CRUD ACTION get
  *   @param ObjectId id Id 
  *
  */
  async get(id) {
    return await generatedModel.model.findOne({_id: id});
  },
  
  /**
  * nominaModel.get
  *   @description CRUD ACTION get
  *   @param filter object
  *
  */
 
 async downloadData(filter) {
    let query={};
    if(filter.datereg_fin&&filter.datereg_ini){
        query.datereg = { $lte : new Date(filter.datereg_fin) , $gte : new Date(filter.datereg_ini)};
    }
    
    if(filter.name){
      query.name = new RegExp(`^${filter.name}`);
    }

    
  return await generatedModel.model.find(query).select(["-_id","-__v"]);
},
  /**
  * nominaModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF nomina
  *
  */
  async list() {
    return await generatedModel.model.find();
  },
  
  /**
  * nominaModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id nomina
  *   @returns nomina
  *
  */
  async update(item) { 
    return await generatedModel.model.findOneAndUpdate({ _id: item._id }, item, {'new': true});
  },
  
};

export default generatedModel;
