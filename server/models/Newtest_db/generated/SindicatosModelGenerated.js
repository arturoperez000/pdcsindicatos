// Database
import Database from "../../../classes/Database_Newtest_db";
import mongoose, { Schema } from "mongoose";

// Logger
import Logger from "../../../classes/Logger";

const generatedModel = {
  /**
   * Init  schema
   */
  init() {
    const db = Database.getConnection();

    /**
      * Sindicatos
      */
    const SindicatosSchema = new mongoose.Schema({
      name: {
        type: "String"
      },
      //RELATIONS
      
      
      //EXTERNAL RELATIONS
      /*
      */
    });

    generatedModel.setModel(db.connection.model("Sindicatos", SindicatosSchema));

    return SindicatosSchema;
  },

  /**
   * Set Model
   */
  setModel: model => {
    generatedModel.model = model;
  },

  /**
   * Get model
   */
  getModel: () => {
    return generatedModel.model;
  },

  // Start queries
    

  // CRUD METHODS


  /**
  * SindicatosModel.create
  *   @description CRUD ACTION create
  *   @param Sindicatos obj Object to insert
  *
  */
  async create(item) {
    const obj = new generatedModel.model(item);
    return await obj.save();
  },
  
  /**
  * SindicatosModel.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id Sindicatos
  *
  */
  async delete(id) {
    return await generatedModel.model.findByIdAndRemove(id);
  },
  
  /**
  * SindicatosModel.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF Sindicatos
  *
  */
  async list() {
    return await generatedModel.model.find();
  },
  
  /**
  * SindicatosModel.update
  *   @description CRUD ACTION update
  *   @param ObjectId id Id Sindicatos
  *   @returns Sindicatos
  *
  */
  async update(item) { 
    return await generatedModel.model.findOneAndUpdate({ _id: item._id }, item, {'new': true});
  },
  


};

export default generatedModel;
