
import ComunasModelGenerated from "./generated/ComunasModelGenerated";
import Logger from "../../classes/Logger";
import lib from "../../_helpers/data";


const customModel = {
  
  /**
   * Customize here your schema with custom attributes
   * 
   * EXAMPLE:
    //*/
    init() {
      let schema = ComunasModelGenerated.init();
  
      this.createComunas();
    },
     
   


    createComunas: async () => {
      const count = await ComunasModelGenerated.getModel().countDocuments();
      if (count == 0) {
        Logger.info("Creating comunas");
        
        lib.getStuff("comunas").then(data => {
          const obj = JSON.parse(data);
          obj.map(
            element=>{
              let comuna = new ComunasModelGenerated.model({
                name: element.name
              });
              ComunasModelGenerated.getModel().create(comuna);
            }
          )
          //Logger.info(data);
        });
        //*/
        /*
        return await admin.save();
        //*/
      }
    }
  /**
   * Override here your custom queries
   * EXAMPLE:
   *
   
    async get(id) {
      console.log("This is my custom query");
      return await ComunasModelGenerated.getModel().findOne({ _id: id });
    }

   */

};

export default {
  ...ComunasModelGenerated,
  ...customModel
};