
import RolesModelGenerated from "./generated/RolesModelGenerated";

import Logger from "../../classes/Logger";
import lib from "../../_helpers/data";

const customModel = {
  
  /**
   * Customize here your schema with custom attributes
   * 
   * EXAMPLE:
   */

    init() {
      let schema = RolesModelGenerated.init();
      
      this.createRoles();
    },
     

    createRoles: async () => {
      const count = await RolesModelGenerated.getModel().countDocuments();
      if (count == 0) {
        Logger.info("Creating roles");
        
        lib.getStuff("roles").then(data => {
          const obj = JSON.parse(data);
          obj.map(
            element=>{
              let rol = new RolesModelGenerated.model({
                name: element.name
              });
              RolesModelGenerated.getModel().create(rol);
            }
          )
          //Logger.info(data);
        });
      }
    }


  /**
   * Override here your custom queries
   * EXAMPLE:
   *
   
    async get(id) {
      console.log("This is my custom query");
      return await RolesModelGenerated.getModel().findOne({ _id: id });
    }

   */

};

export default {
  ...RolesModelGenerated,
  ...customModel
};