
import NominaModelGenerated from "./generated/NominaModelGenerated";

const customModel = {
  
  /**
   * Customize here your schema with custom attributes
   * 
   * EXAMPLE:
    
    init() {
      let schema = NominaModelGenerated.init();
  
      schema.add({
        extraCustomField: Object
      });
    },
     
   */


  /**
   * Override here your custom queries
   * EXAMPLE:
   *
   
    async get(id) {
      console.log("This is my custom query");
      return await NominaModelGenerated.getModel().findOne({ _id: id });
    }

   */

};

export default {
  ...NominaModelGenerated,
  ...customModel
};