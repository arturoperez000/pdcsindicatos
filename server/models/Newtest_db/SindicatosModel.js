
import SindicatosModelGenerated from "./generated/SindicatosModelGenerated";

import Logger from "../../classes/Logger";
import lib from "../../_helpers/data";

const customModel = {
  
  /**
   * Customize here your schema with custom attributes
   * 
   * EXAMPLE:
    //*/
    init() {
      let schema = SindicatosModelGenerated.init();
  
      this.createSindicatos();
    },
     
   
    createSindicatos: async () => {
      const count = await SindicatosModelGenerated.getModel().countDocuments();
      if (count == 0) {
        Logger.info("Creating sindicatos");
        
        lib.getStuff("sindicatos").then(data => {
          const obj = JSON.parse(data);
          obj.map(
            element=>{
              let sindicato = new SindicatosModelGenerated.model({
                name: element.name
              });
              SindicatosModelGenerated.getModel().create(sindicato);
            }
          )
          //Logger.info(data);
        });
      }
    }

  /**
   * Override here your custom queries
   * EXAMPLE:
   *
   
    async get(id) {
      console.log("This is my custom query");
      return await SindicatosModelGenerated.getModel().findOne({ _id: id });
    }

   */

};

export default {
  ...SindicatosModelGenerated,
  ...customModel
};