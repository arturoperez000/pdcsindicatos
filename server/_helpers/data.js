const fs = require('fs');
var path = require('path');
const util = require('util');


// Convert fs.readFile into Promise version of same    
const readFile = util.promisify(fs.readFile);

var lib = {};
lib.baseDir = path.join(__dirname,'/../.data/')

lib.getStuff = async function (fileName) {
  return await readFile(lib.baseDir+fileName+'.json');
}

// Can't use `await` outside of an async function so you need to chain
// with then()
/*
lib.getStuff().then(data => {
  console.log(data);
})
//*/

module.exports = lib;