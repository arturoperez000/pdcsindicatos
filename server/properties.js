
module.exports = {
	newtest_db_dbUrl: 'localhost:27017/sindicatos_db',
    publicPath: '../client/dist',
	port: 8000,
    tokenSecret: 'Insert Your Secret Token',
    api: '/api'
}