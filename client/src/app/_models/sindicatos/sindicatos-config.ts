import { Sindicatos } from "./Sindicatos";

import { Validators } from '@angular/forms';
import { FieldConfig } from "../../_directives/dynamic-form/models/field-config.interface";

//Campos necesarios
const config: FieldConfig[] = [
    
  {
    type: 'input',
    label: 'Nombre completo',
    name: 'name',
    placeholder: 'Introduzca el nombre',
    validation: [Validators.required]
  },
  {
    type: 'input',
    label: 'Descripcion',
    name: 'description',
    placeholder: 'Introduzca la descripcion',
    validation: [Validators.required]
  },
  {
    label: 'Submit',
    name: 'submit',
    type: 'button'
  },
];

export class SindicatosConfig  {
  //get config() { return config.filter(({type}) => type !== 'button'); }

    sindicatos;

    constructor(){
        this.sindicatos = config;
    }
  
  setValues(sindicatos: Sindicatos){
    Object.keys(sindicatos).forEach(keys=>
                              this.sindicatos=config.filter(
                                        (sindicatosConfig)=>{
                                          if(keys==sindicatosConfig.name){
                                            sindicatosConfig.value = sindicatos[keys]
                                          }
                                          return sindicatos
                                        }
                                    )
                              )
    }

    config(){
      return this.sindicatos;
    }
  }
