

export class Comunas  {

    public _id;
    public name;

    constructor(
        _id?: string,
        name?: string
    ) {
        this._id = _id;
        this.name = name;
    }

}
