
export class Nomina {


    public _id?: string;
    public name?: string;
    public last?: string;
    public rut?: string;
    public dig?: string;
    public datenac?: string;
    public street_num?: string;
    public comuna?: string;
    public servicio?: String
    public movil?: string;
    public email?: string;
    public dateinemp?: Date;
    public sindicato?: string;
    public datereg?: Date;
    public monto?: string;


    constructor(
        _id?: string,
        name?: string,
        last?: string,
        rut?: string,
        dig?: string,
        datenac?: string,
        street_num?: string,
        comuna?: string,
        servicio?: String,
        movil?: string,
        email?: string,
        dateinemp?: Date,
        sindicato?: string,
        datereg?: Date,
        monto?: string
    ) {
        this._id = _id;
        this.name = name;
        this.last = last;
        this.rut = rut;
        this.dig = dig;
        this.datenac = datenac;
        this.street_num = street_num;
        this.comuna = comuna;
        this.servicio = servicio;
        this.movil = movil;
        this.email = email;
        this.dateinemp = dateinemp;
        this.sindicato = sindicato;
        this.datereg = datereg;
        this.monto = monto;
    }

}
