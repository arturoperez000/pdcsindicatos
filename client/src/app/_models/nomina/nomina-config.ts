import { Nomina } from "./nomina";

import { Validators } from '@angular/forms';
import { FieldConfig } from "../../_directives/dynamic-form/models/field-config.interface";

//Campos necesarios
const config: FieldConfig[] = [
    
  {
    type: 'input',
    label: 'Nombre',
    name: 'name',
    placeholder: 'Buscar por nombre'
  },
  {
    type: 'date',
    label: 'Fecha de registro',
    name: 'datereg',
    placeholder: 'Buscar por fecha de registro'
  },
  {
    label: 'Export',
    name: 'submit',
    type: 'button'
  },
];

export class NominaConfig  {
  //get config() { return config.filter(({type}) => type !== 'button'); }

  nomina;

    constructor(
      ){
        this.nomina = config;
      }
      
  setValues(nomina: Nomina){
    Object.keys(nomina).forEach(keys=>
                              this.nomina=config.filter(
                                        (nominaConfig)=>{
                                          if(keys==nominaConfig.name){
                                            nominaConfig.value = nomina[keys]
                                          }
                                          return nomina
                                        }
                                    )
                              )
    }

    config(){
      return this.nomina;
    }
  }
