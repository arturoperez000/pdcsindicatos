import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '',
    title: 'Users',
    icon: 'mdi mdi-dots-horizontal',
    class: 'nav-small-cap',
    extralink: true,
    submenu: []
  },
  {
    path: 'user',
    title: 'Lista de usuarios',
    icon: 'fas fa-user',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: 'roles',
    title: 'Control de roles',
    icon: 'fa fa-id-badge',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '',
    title: 'Sindicato',
    icon: 'mdi mdi-dots-horizontal',
    class: 'nav-small-cap',
    extralink: true,
    submenu: []
  },
  {
    path: 'nomina',
    title: 'Nomina',
    icon: 'fas fa-address-book',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: 'sindicatos',
    title: 'Control de sindicatos',
    icon: 'far fa-list-alt',
    class: '',
    extralink: false,
    submenu: []
  },
];
