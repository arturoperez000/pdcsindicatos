import { Component, EventEmitter, Output } from '@angular/core';
import { AuthenticationService } from '../../../_auth/authentication.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html'
})
export class TopbarComponent {
  @Output()
  toggleSidebar = new EventEmitter<void>();

  collapsed = true;

  constructor(
    private authenticationService : AuthenticationService
  ) {}

  
  onLogout() {    
    this.authenticationService.logout();
  }
}
