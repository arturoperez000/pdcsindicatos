import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NominaEditPageComponent } from './nominaeditpage.component';

const routes: Routes = [
  {
    path: '',
    component: NominaEditPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class NominaEditPageRoutingModule { }
