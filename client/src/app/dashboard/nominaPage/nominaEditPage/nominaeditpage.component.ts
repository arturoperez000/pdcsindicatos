import { Component, OnInit }        from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';

import { Nomina } from '../../../_models/nomina/nomina'
import { ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { AlertService, NominaService } from 'src/app/_services';
import { ComunasService } from 'src/app/_services/comunas.service';
import { SindicatosService } from 'src/app/_services/sindicatos.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NominaConfig } from '../../../_models/nomina/nomina-config'
import { DynamicFormComponent } from '../../../_directives/dynamic-form/dynamic-form.component'
//import { UserFormService } from './UserForm.service';

@Component({
  selector: 'app-nominaeditpage',
  templateUrl: './nominaeditpage.component.html',
  styleUrls: ['./nominaeditpage.component.scss']
  
})

export class NominaEditPageComponent {

    nomina: Nomina;
    comunas = [];
    sindicatos = [];
    constructor(
      private fb: FormBuilder,
      private comunasService: ComunasService,
      private sindicatosService: SindicatosService,
      private nominaService: NominaService,
      private router: Router,
      private route: ActivatedRoute
  ) { 
    this.comunasService.getAll().subscribe(response=>{
                                                            this.comunas=response;
                                                        }
                                                );    
    this.sindicatosService.getAll().subscribe(response=>{
                                                            this.sindicatos=response;
                                                        }
                                                );    
  }

  ngOnInit(): void {

      this.route.params.subscribe(params => {

          if (params.id === 'new') {
              // New User

              this.nomina = new Nomina(null, null, null, null, null, null, null, null, null, null, null, null, null, null , null);
              
          } else {
              // Get User
              this.nominaService.getById(params.id).subscribe(nomina => {
                  this.nomina = nomina;
              });
          }
      });
  }

  

  /**
   * Save or create User
   */
  save(): void {
      if (this.nomina._id) {
          // Save
          this.nominaService.update(this.nomina).subscribe(data => this.router.navigateByUrl('/dashboard/nomina'));
      } else {
          // Create
          this.nominaService.register(this.nomina).subscribe(data => this.router.navigateByUrl('/dashboard/nomina'));
      }
  }

}
