import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NominaEditPageRoutingModule } from './nominaeditpage-routing.module';
import { SharedModule } from '../../../shared.module';
import { NominaEditPageComponent } from './nominaeditpage.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NominaService } from '../../../_services/nomina.service';
import { ComunasService } from 'src/app/_services/comunas.service';
import { SindicatosService } from 'src/app/_services/sindicatos.service';
import { NominaConfig } from 'src/app/_models/nomina/nomina-config';

@NgModule({
  imports: [
    CommonModule,
    NominaEditPageRoutingModule,
    FormsModule,
    SharedModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    NominaEditPageComponent
  ],
  providers:[NominaConfig,NominaService,ComunasService,SindicatosService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NominaEditPageModule { }