import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NominaPageRoutingModule } from './nominapage-routing.module';
import { SharedModule } from '../../shared.module';
import { NominaPageComponent } from './nominapage.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NominaService } from '../../_services/nomina.service';
import { NominaConfig } from 'src/app/_models/nomina/nomina-config';
@NgModule({
  imports: [
    CommonModule,
    NominaPageRoutingModule,
    FormsModule,
    SharedModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    NominaPageComponent
  ],
  providers:[NominaConfig,NominaService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NominaPageModule { }