import { Component, OnInit, AfterViewInit }        from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { AlertService, NominaService } from 'src/app/_services';
import { NominaConfig } from '../../_models/nomina/nomina-config'
import { DynamicFormComponent } from '../../_directives/dynamic-form/dynamic-form.component'
import { Nomina } from "../../_models/nomina/nomina";
import { saveAs } from 'file-saver';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-nominaPage',
  templateUrl: './nominapage.component.html',
  styleUrls: ['./nominapage.component.scss']
  
})

export class NominaPageComponent implements AfterViewInit {
    
  public nomina: Nomina[] =[];
  
  idSelected;
  config;

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  //@ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  ngAfterViewInit() {
    console.log(this.form);
  }
  /*
  formSubmit(value) {    
    //console.log(value)
    this.NominaService.register(value)
    .subscribe(
        data => {
          this.alertService.success("Se agrego un nuevo rol");
          this.getNomina();
          //this.form.controls;
        },
        error => {
            this.alertService.error(error);
        });
    
  }
  //*/
  constructor(
                private NominaService: NominaService,
                private alertService: AlertService, 
                private nominaConfig: NominaConfig
              ) 
    {
    this.nominaConfig = new NominaConfig();
    this.config = this.nominaConfig.config();       
    this.getNomina();
    }


  getNomina(){
    this.NominaService.getAll()
    .subscribe(
        data => {
          // console.log(data);
          this.nomina = data;
        },
        error => {
         // console.log(error);
            this.alertService.error(error);
        });
  }

  formSubmit(value) {
    //let cvs = this.filter(this.nomina, value);
    let filter = {}; // =  Object.entries(value).map(a=>a!=undefined);
    console.log(value);
    Object.keys(value).forEach(key=>{                                      
                                      if(value[key]){
                                        if (typeof value[key] === 'object') {
                                          filter[key+'_ini']= value[key]; 
                                          filter[key+'_fin']= new Date(Date.parse(value[key]) + (24*60*60*1000));
                                        }
                                        if (typeof value[key] === 'string') {
                                          filter[key] = value[key];
                                        }
                                      }
                                    });

    this.NominaService.exportCvs(filter).subscribe(resp=>{
      let keys = Object.keys(resp[0]);
      let file = keys.join();
      file += "\n"
      resp.forEach(value=>{
        file += keys.map(key=>{
          return value[key];
        }).join()
        file += "\n"
      })
      var csvBody = new Blob([file], {type : 'text/csv'}); // the blob
      saveAs(csvBody, "nomina-"+Date.now()+".csv");
      // access the body directly, which is typed as `Config`.
      //this.config = { ... resp.body };
    });

  }
  /**
   * Select Weas to remove
   *
   * @param {string} id Id of the Weas to remove
   */
  selectId(id: string) {
    this.idSelected = id;
  }

  deleteNomina(id) {
    this.NominaService.delete(id)
    .subscribe(
        data => {
          this.alertService.success("Eliminado Exitoso");
          this.getNomina();
        },
        error => {
            this.alertService.error(error);
        });
  }


}