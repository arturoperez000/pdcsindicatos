import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NominaPageComponent } from './nominapage.component';

const routes: Routes = [
  {
    path: '',
    component: NominaPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class NominaPageRoutingModule { }
