import { Component, OnInit, HostListener } from '@angular/core';
/*
declare var $: any
declare var require: any;

import {GLOBAL} from './../../config/config'
import { Services } from './../services/services';
import {Observable} from 'rxjs/Observable';
import { Router, NavigationStart, Event as NavigationEvent } from '@angular/router';
import { ConsolidadoModel } from '../ausentismo/ausentismo/consolidado.model';
//*/

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.html',
  styles: ['./dashboard.component.scss'],
  providers: [],
})
export class DashboardComponent implements OnInit {

  public innerWidth: any;
  public defaultSidebar: any;
  public sidebartype = 'full';
  public expandLogo = false;

  Logo() {
    this.expandLogo = !this.expandLogo;
  }
    ngOnInit(){
      this.defaultSidebar = this.sidebartype;
      this.handleSidebar();
    }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleSidebar();
  }  

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.sidebartype = 'mini-sidebar';
    } else {
      this.sidebartype = this.defaultSidebar;
    }
  }

  
  toggleSidebarType() {
    switch (this.sidebartype) {
      case 'full':
        this.sidebartype = 'mini-sidebar';
        break;

      case 'mini-sidebar':
        this.sidebartype = 'full';
        break;

      default:
    }
  }
}