import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SindicatosPageComponent } from './sindicatospage.component';

const routes: Routes = [
  {
    path: '',
    component: SindicatosPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class SindicatosPageRoutingModule { }
