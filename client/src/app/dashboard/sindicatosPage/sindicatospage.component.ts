import { Component, OnInit }        from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { ViewChild, AfterViewInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AlertService, SindicatosService } from 'src/app/_services';
import { SindicatosConfig } from '../../_models/sindicatos/sindicatos-config'
import { DynamicFormComponent } from '../../_directives/dynamic-form/dynamic-form.component'

@Component({
  selector: 'app-sindicatosPage',
  templateUrl: './sindicatosPage.component.html',
  styleUrls: ['./sindicatosPage.component.scss']
  
})

export class SindicatosPageComponent implements AfterViewInit {
    
  public sindicatos=[];
  
  idSelected;
  //sindicatosConfig;
  config;

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  formSubmit(value) {    
    console.log(value)
    this.SindicatosService.register(value)
    .subscribe(
        data => {
          this.alertService.success("Se agrego un nuevo rol");
          this.getSindicatos();
          //this.form.controls;
        },
        error => {
            this.alertService.error(error);
        });
    //*/
  }
  
  constructor(
                private SindicatosService: SindicatosService,
                private alertService: AlertService, 
                private sindicatosConfig: SindicatosConfig
              ) 
    {
    this.sindicatosConfig = new SindicatosConfig();
    this.config = this.sindicatosConfig.config();       
    this.getSindicatos();
    }

  ngAfterViewInit() {    
    let previousValid = this.form.valid;
    
    this.form.changes.subscribe(() => {
      if (this.form.valid !== previousValid) {
        previousValid = this.form.valid;
        this.form.setDisabled('submit', !previousValid);
      }
    });
    this.form.setDisabled('submit', true);
  }

  getSindicatos(){
    this.SindicatosService.getAll()
    .subscribe(
        data => {
          // console.log(data);
          this.sindicatos =data;
        },
        error => {
         // console.log(error);
            this.alertService.error(error);
        });
  }

  /**
   * Select sindicatos to remove
   *
   * @param {string} id Id of the sindicatos to remove
   */
  selectId(id: string) {
    this.idSelected = id;
  }

  deleteSindicatos(id) {
    this.SindicatosService.delete(id)
    .subscribe(
        data => {
          this.alertService.success("Eliminado Exitoso");
          this.getSindicatos();
        },
        error => {
            this.alertService.error(error);
        });
  }
}