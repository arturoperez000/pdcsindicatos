import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SindicatosPageRoutingModule } from './sindicatospage-routing.module';
import { SharedModule } from '../../shared.module';
import { SindicatosPageComponent } from './sindicatospage.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SindicatosService } from '../../_services/sindicatos.service';
import { SindicatosConfig } from 'src/app/_models/sindicatos/sindicatos-config';

@NgModule({
  imports: [
    CommonModule,
    SindicatosPageRoutingModule,
    FormsModule,
    SharedModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    SindicatosPageComponent
  ],
  providers:[SindicatosConfig,SindicatosService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SindicatosPageModule { }