import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RolesPageRoutingModule } from './rolespage-routing.module';
import { SharedModule } from '../../shared.module';
import { RolesPageComponent } from './rolespage.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RolesService } from '../../_services/roles.service';
import { RolesConfig } from 'src/app/_models/roles/roles-config';

@NgModule({
  imports: [
    CommonModule,
    RolesPageRoutingModule,
    FormsModule,
    SharedModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    RolesPageComponent
  ],
  providers:[RolesConfig,RolesService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RolesPageModule { }