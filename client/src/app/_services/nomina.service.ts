import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Nomina } from '../_models/nomina/nomina';
import { GLOBAL as config } from '../_config/config';
import { Config } from '../_models/formconfig';
//import { config } from 'rxjs';

@Injectable()
export class NominaService {
    constructor(private http: HttpClient) { 
                }

    contextUrl: string = config.url+'nominas';

    exportCvs(filter ): Observable<Nomina[]>{
        return this.http.post<Nomina[]>(`${this.contextUrl}/export`, filter)
        .pipe(map(data => data));        
        
        //*/
    }

    getAll(): Observable<Nomina[]> {
        return this.http.get<Nomina[]>(`${this.contextUrl}`)
        .pipe(map(data => data));
    }

    getById(id: string): Observable<Nomina> {
        return this.http
        .get<Nomina>(`${this.contextUrl}/` + id)
        .pipe(map(data => data));
    }

    register(nomina: Nomina): Observable<Nomina> {
        return this.http.post<Nomina>(`${this.contextUrl}/`, nomina)
        .pipe(map(data => data));;
    }

    update(nomina: Nomina) {
        return this.http.post(`${this.contextUrl}/` + nomina._id, nomina)
        .pipe(map(data => data));
    }

    delete(id: string): Observable<void> {
        return this.http.delete<void>(`${this.contextUrl}/` + id)
        .pipe(map(data => data));
    }
}