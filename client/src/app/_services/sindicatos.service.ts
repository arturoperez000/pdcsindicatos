import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Sindicatos } from '../_models/sindicatos/sindicatos';
import { GLOBAL as config } from '../_config/config';
//import { config } from 'rxjs';

@Injectable()
export class SindicatosService {
    constructor(private http: HttpClient) { 
                }

    contextUrl: string = config.url+'sindicatos';

    getAll(): Observable<Sindicatos[]> {
        return this.http.get<Sindicatos[]>(`${this.contextUrl}`)
        .pipe(map(data => data));
    }

    getById(id: string): Observable<Sindicatos> {
        return this.http
        .get<Sindicatos>(`${this.contextUrl}/` + id)
        .pipe(map(data => data));
    }

    register(Sindicatos: Sindicatos): Observable<Sindicatos> {
        return this.http.post<Sindicatos>(`${this.contextUrl}/`, Sindicatos)
        .pipe(map(data => data));;
    }

    update(Sindicatos: Sindicatos) {
        return this.http.put(`${this.contextUrl}/` + Sindicatos._id, Sindicatos)
        .pipe(map(data => data));
    }

    delete(id: string): Observable<void> {
        return this.http.delete<void>(`${this.contextUrl}/` + id)
        .pipe(map(data => data));
    }
}