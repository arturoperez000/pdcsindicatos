import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Comunas } from '../_models/comunas/comunas';
import { GLOBAL as config } from '../_config/config';
//import { config } from 'rxjs';

@Injectable()
export class ComunasService {
    constructor(private http: HttpClient) { 
                }
            
    contextUrl: string = config.url+'comunas';

    getAll(): Observable<Comunas[]> {
        return this.http.get<Comunas[]>(`${this.contextUrl}`)
        .pipe(map(data => data));
    }

}