import { Component, ViewContainerRef } from '@angular/core';
import { FieldConfig } from '../../models/field-config.interface';
import { FormGroup } from '@angular/forms';
import { Field } from '../../models/field.interface';


@Component({
  selector: 'form-input',
  styleUrls: ['form-input.component.scss'],
  template: `
    <mat-form-field 
      [formGroup]="group">
      <mat-label class="float-left">{{ config.label }}</mat-label>
      <input
        matInput
        mdbValidate 
        type="text" 
        [attr.placeholder]="config.placeholder"
        [formControlName]="config.name" >          
      <mat-error *ngIf="validate.invalid && (validate.dirty || validate.touched)">Input invalid</mat-error>
    </mat-form-field>
  `,
})
export class FormInputComponent implements Field  {
  config: FieldConfig;
  group: FormGroup;
  
  get validate() { return this.group.get(this.config.name); }
}