import { Component, ViewContainerRef } from '@angular/core';
import { FieldConfig } from '../../models/field-config.interface';
import { FormGroup } from '@angular/forms';
import { Field } from '../../models/field.interface';


@Component({
  selector: 'form-date',
  styleUrls: ['form-date.component.scss'],
  template: `
    <mat-form-field
      [formGroup]="group">
      <mat-label class="float-left">{{ config.label }}</mat-label>
      <input matInput [matDatepicker]="picker" placeholder="{{config.placeholder}}"
      [formControlName]="config.name">
      <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
      <mat-datepicker #picker></mat-datepicker>
    </mat-form-field>
  `,
})
export class FormDateComponent implements Field  {
  config: FieldConfig;
  group: FormGroup;
  
  get validate() { return this.group.get(this.config.name); }
}